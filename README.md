This repository contains R scripts to generate visualizations of basic concepts in probability and statistics

**Contents**

1. Visualizing coin toss experiments: 
  - [R script](coin-toss-animations.R)
  - <a href="https://dododas.gitlab.io/post/2017-02-03-coin-toss-visualization/" target="_blank">Blog post</a>

2. Visualizing the Binomial distribution:
  - [R script](binomial-animation.R)
  - <a href="https://dododas.gitlab.io/post/2017-02-19-binomial-visualization/" target="_blank">Blog post</a>
